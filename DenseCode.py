import cPickle as pickle

tracks_dict = {}
tags_dict = {}

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []
		
class Tag:
	def __init__(self, name):
	  self.name = name
	  self.associatedSongs = []
		
with open("TrimmedTagsTracks.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
		
with open("TrimmedTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tags_dict[value.name] = value


def writeObjectsToFile(name):
	count = 0
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(tracks_dict), output)
		for track in tracks_dict:
			print(count)
			count += 1
			pickle.dump(tracks_dict[track], output)
		
sorted_tags_list = []
for key in tags_dict:
	tag = tags_dict[key]
	sorted_tags_list.append(tag.name)

with open("SortedTags.pkl", 'wb') as output:
    pickle.dump(sorted_tags_list, output) 

count = 0
for key in tracks_dict:
	print(count)
	count += 1
	track = tracks_dict[key]
	temp_tag_list = []
	for i in sorted_tags_list:
		if(i in track.tags):
			temp_tag_list.append(1)
		else:
			temp_tag_list.append(0)
	track.tags = temp_tag_list
			
writeObjectsToFile("DenseTrimmedTagsTracks")
