import requests
import json
import cPickle as pickle

API_KEY = "92651e3ec89a548acf57d253b98d1f59"
# (username: object)
users_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

def getResponse(url):
	response = requests.get(url)
	if response.status_code == 200:
		jsonResponse = json.loads(response.text)
		return jsonResponse	
	else:
		print("Invalid URL")

def getUserFriends(username):
	url = "http://ws.audioscrobbler.com/2.0/?method=user.getfriends&user=" + username + "&api_key=" + API_KEY + "&format=json"
	jsonResponse = getResponse(url)
	friendList = []
	try:
		jsonFriendList = jsonResponse['friends']['user']
		for i in jsonFriendList:
	  		friendList.append(i['name'])
	except:
		pass
	return friendList
	
def getUserTopTracks(username):
	url = "http://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user=" + username + "&api_key=" + API_KEY + "&format=json"
	jsonResponse = getResponse(url)
	jsonTopTracksList = jsonResponse['toptracks']['track']
	topTracksList = []
	for i in jsonTopTracksList:
		if(i['mbid'] != "" and i['mbid'] != None):
			topTracksList.append(i['mbid'])
	return topTracksList
  
def getUserLovedTracks(username):
	url = "http://ws.audioscrobbler.com/2.0/?method=user.getlovedtracks&user=" + username + "&api_key=" + API_KEY + "&format=json"
	jsonResponse = getResponse(url)
	jsonLovedTracksList = jsonResponse['lovedtracks']['track']
	lovedTracksList = []
	for i in jsonLovedTracksList:
		if(i['mbid'] != "" and i['mbid'] != None):
			lovedTracksList.append(i['mbid'])
	return lovedTracksList

MAX_USER_COUNT = 1968
MAX_FRIEND_COUNT = 15
def getUserInfo(username, div):
	global count
	if(count > MAX_USER_COUNT):
		return
	print(count)
	url = "http://ws.audioscrobbler.com/2.0/?method=user.getinfo&user=" + username + "&api_key=" + API_KEY + "&format=json"
	jsonResponse = getResponse(url)
	user = User(jsonResponse['user']['name'])
	user.friends = getUserFriends(username)
	user.topTracks = getUserTopTracks(username)
	user.lovedTracks = getUserLovedTracks(username)
	users_dict[username] = user
	count += 1
	friendCount = 0
	for friend in user.friends:
		if(friendCount > MAX_FRIEND_COUNT/div):
			break
		if(not friend in users_dict):
			friendCount += 1
			getUserInfo(friend, div*2)

def writeObjectsToFile(name):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(users_dict), output)
		for user in users_dict:
			pickle.dump(users_dict[user], output)

count = 0
getUserInfo("rj", 1)
writeObjectsToFile("Users")