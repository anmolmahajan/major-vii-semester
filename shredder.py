import cPickle as pickle

tracks_dict = {}
tags_dict = {}

class Tag:
	def __init__(self, name):
		self.name = name
		self.associatedSongs = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

with open("TrimmedTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tags_dict[value.name] = value

with open("Old Data/TrimmedTagsTracks.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value

def writeObjectsToFile(name, dictionary):
	count = 0
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(dictionary), output)
		for key in dictionary:
			pickle.dump(dictionary[key], output)
		
		
sorted_tags_list = []
remove = []
for key in tags_dict:
	tag = tags_dict[key]
	if(len(tag.associatedSongs) <= 10):
		remove.append(tag.name)
	else:
		sorted_tags_list.append(tag.name)

for i in remove:
	del(tags_dict[i])

print(len(tags_dict))

remove = []
count = 0
for key in tracks_dict:
	track = tracks_dict[key]
	temp_tag_list = []
	flag = 0
	for i in sorted_tags_list:
		if(i in track.tags):
			temp_tag_list.append(1)
			flag = 1
		else:
			temp_tag_list.append(0)
	track.tags = temp_tag_list
	if(flag == 0):
		remove.append(track.id)

print(len(remove))

for i in remove:
	del(tracks_dict[i])

with open("SortedTags.pkl", 'wb') as output:
	pickle.dump(sorted_tags_list, output)

	
writeObjectsToFile("TracksWithLessTags", tracks_dict)
writeObjectsToFile("LessTags",tags_dict)