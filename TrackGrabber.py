import cPickle as pickle
import requests
import json

API_KEY = "92651e3ec89a548acf57d253b98d1f59"

users_dict = {}
tracks_dict = {}

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []				
				
class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []
				
def getResponse(url):
	response = requests.get(url)
	if response.status_code == 200:
		jsonResponse = json.loads(response.text)
		return jsonResponse	
	else:
		print("Invalid URL")

def readObjectsFromFile(name):
	with open(name + ".pkl", "rb") as infile:
		for i in range(pickle.load(infile)):
			user = pickle.load(infile)
			users_dict[user.username] = user

						
def getTrackInfo(mbid, flag):
		global tracker
		if(mbid in tracks_dict):
			return
		tracker += 1
		print(tracker)
		url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&api_key=" + API_KEY + "&mbid=" + mbid + "&format=json"
		jsonResponse = getResponse(url)
		track = Track(jsonResponse['track']['mbid'])
		track.name = jsonResponse['track']['name']
		track.artistId = jsonResponse['track']['artist']['mbid']
		for tag in jsonResponse['track']['toptags']['tag']:
			track.tags.append(tag['name'])
		tracks_dict[track.id] = track
		
		url = "http://ws.audioscrobbler.com/2.0/?method=track.getsimilar&mbid=" + mbid + "&api_key=" + API_KEY + "&format=json"
		jsonResponse = getResponse(url)
		count = 0
		if(flag):
			for item in jsonResponse['similartracks']['track']:
					flag = False
					count += 1
					if(count < 3):
						try:
							getTrackInfo(item['mbid'], flag)
						except:
							pass

def writeObjectsToFile(name):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(tracks_dict), output)
		for track in tracks_dict:
			pickle.dump(tracks_dict[track], output)

readObjectsFromFile("Users")            

tracker = 0
for key in users_dict:
		user = users_dict[key]
		for track in user.lovedTracks + user.topTracks:
				getTrackInfo(track, True)

writeObjectsToFile("Tracks")