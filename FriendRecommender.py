import cPickle as pickle
import warnings
import time
from sklearn.cluster import KMeans

def warn(*args, **kwargs):
	pass
warnings.warn = warn

users_dict = {}
tracks_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []
		
class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

class Cluster:
	def __init__(self, label):
		self.label = label
		self.memberUsers = []

print("Reading Users")
with open('Users.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		user = pickle.load(infile)
		users_dict[user.username] = user

print("Reading Tracks")
with open('TracksWithLessTags.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		track = pickle.load(infile)
		tracks_dict[track.id] = track

for key in users_dict:
	user = users_dict[key]
	user.tags = [0] * 401
	for trackKey in list(set((user.topTracks + user.lovedTracks))):
		if(not trackKey in tracks_dict):
			continue
		track = tracks_dict[trackKey]
		for i in range(len(track.tags)):
			user.tags[i] += track.tags[i]

def getEuclideanDistance(user1, user2):
	tags1 = user1.tags
	tags2 = user2.tags
	distance = 0.0
	for i in range(len(tags1)):
		distance += float(((tags1[i]-tags2[i])**2))
	return distance**0.5

print("POPULATING X")
X = []
for key in users_dict:
	user = users_dict[key]
	X.append(user.tags)

starttime = time.time()
kmeans = KMeans(n_clusters=15, random_state=0, n_jobs=-2)
kmeans = kmeans.fit(X)
print("Clustering : " + str(time.time() - starttime))	

clusters_dict = {}
for key in users_dict:
	user = users_dict[key]
	label = kmeans.predict(user.tags)[0]
	if(not label in clusters_dict):
		clusters_dict[label] = Cluster(label)
	clusters_dict[label].memberUsers.append(user.username)

for label in clusters_dict:
	print clusters_dict[label].memberUsers

print("\n\n")

for key in users_dict:
	user = users_dict[key]
	if(len(user.friends)<30):
		continue
	cluster = clusters_dict[kmeans.predict(user.tags)[0]]
	
	minD = 10000000
	minUser = ""
	for i in cluster.memberUsers:
		user2 = users_dict[i]
		if(user2.username == user.username):
			continue
		ed = getEuclideanDistance(user,user2)
		if(ed < minD):
			minD = ed
			minUser = i
	
	print("User: " + key),
	print(", MinUser: " + minUser),
	print(", MinDistance: " + str(minD)),
	print(", " + str(minUser in user.friends))
	
