from multiprocessing import Pool
import requests
import cPickle as pickle
import json
import time

users_dict = {}
tracks_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

def readObjectsFromFile(name):
	with open(name + ".pkl", "rb") as infile:
		for i in range(pickle.load(infile)):
			user = pickle.load(infile)
			users_dict[user.username] = user

def getResponse(url):
	response = requests.get(url)
	if response.status_code == 200:
		jsonResponse = json.loads(response.text)
		return jsonResponse	
	else:
		print("Invalid URL")

def getTrackInfo(url):
	try:
		jsonResponse = getResponse(url)
	except:
		return ("-1", Track("-1"))
	time.sleep(0.1)
	try:
		track = Track(jsonResponse['track']['mbid'])
		track.name = jsonResponse['track']['name']
		track.artistId = jsonResponse['track']['artist']['mbid']
		for tag in jsonResponse['track']['toptags']['tag']:
			track.tags.append(tag['name'])
		return (track.id, track)
	except:
		return ("-1", Track("-1"))
	
def writeObjectsToFile(name, dictionary):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(dictionary), output)
		for track in dictionary:
			pickle.dump(dictionary[track], output)
  
urls = []
print("Starting")
readObjectsFromFile("Users")
print("Done Reading")

API_KEY = "92651e3ec89a548acf57d253b98d1f59"

"""
user = users_dict["RJ"]
for track in user.lovedTracks:
	url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&mbid=" + track + "&api_key=" + API_KEY + "&format=json"
	urls.append(url)
	
"""
for key in users_dict:
	user = users_dict[key]
	for track in user.lovedTracks:
		url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&mbid=" + track + "&api_key=" + API_KEY + "&format=json"
		urls.append(url)


print("URLS done")
p = Pool(4)

print("Starting url loved multi")
tracks_dict = dict(p.map(getTrackInfo, urls))
print(len(tracks_dict))
print("Writing Loved Tracks")
writeObjectsToFile("Tracks", tracks_dict)

urls = []

for key in users_dict:
	user = users_dict[key]
	for track in user.topTracks:
		url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&mbid=" + track + "&api_key=" + API_KEY + "&format=json"
		urls.append(url)

"""
user = users_dict["RJ"]
for track in user.topTracks:
	url = "http://ws.audioscrobbler.com/2.0/?method=track.getInfo&mbid=" + track + "&api_key=" + API_KEY + "&format=json"
	urls.append(url)
"""

print("Starting url top multi")
tracks_dict2 = dict(p.map(getTrackInfo, urls))
print(len(tracks_dict2))
print("Writing Top Tracks")
writeObjectsToFile("Tracks2", tracks_dict2)

