import cPickle as pickle
import time
import warnings
from sklearn.cluster import KMeans

tracks_dict = {}
users_dict = {}

def warn(*args, **kwargs):
	pass
import warnings
warnings.warn = warn

def getSimilarity(A,B):
	numerator = 0
	for i in range(len(clusters_dict[A].tagList)):
		numerator += (clusters_dict[A].tagList[i]*clusters_dict[B].tagList[i])
	denA = 0
	denB = 0
	for i in range(len(clusters_dict[A].tagList)):
		denA += (clusters_dict[A].tagList[i]**2)
		denB += (clusters_dict[B].tagList[i]**2)
	denA = denA ** 0.5
	denB = denB ** 0.5
	return numerator/(denA * denB)

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

print("READING TRACKS")
starttime = time.time()
with open("TracksWithLessTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
		
print("READING USERS")
with open("Users.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		users_dict[value.username] = value

print("Reading: " + str(time.time() - starttime))


X = []
i = 0
starttime = time.time()

track_list = []

print("POPULATING X")
for key in tracks_dict:
	track_list.append(key)
	track = tracks_dict[key]
	X.append(track.tags)
print("Populating: " + str(time.time() - starttime))

f = open('KMeansClassifier.pkl', 'rb')
kmeans = pickle.load(f)
f.close()

clusters_dict = {}

class Cluster:
	def __init__(self, label, tagList):
		self.label = label
		self.tagList = tagList
		self.memberTracks = []
		self.dissimilarCluster = (-1, 0.0)
		
		
for key in tracks_dict:
	track = tracks_dict[key]
	label = kmeans.predict(track.tags)[0]
	
	if(not label in clusters_dict):
		clusters_dict[label] = Cluster(label, track.tags)
	else:
		for i in range(len(track.tags)):
			clusters_dict[label].tagList[i] += track.tags[i]

for i in clusters_dict:
	cluster = clusters_dict[i]
	min = 1000000
	for j in clusters_dict:
		if(getSimilarity(i, j) < min):
			min = getSimilarity(i, j)
			cluster.dissimilarCluster = (j, min)

for i in range(len(kmeans.labels_)):
	clusters_dict[kmeans.labels_[i]].memberTracks.append(track_list[i]) 

def writeObjectsToFile(name, dictionary):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(dictionary), output)
		for i in dictionary:
			pickle.dump(dictionary[i], output)
			
writeObjectsToFile("Clusters", clusters_dict)

with open('Clusters.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		cluster = pickle.load(infile)
		print(str(cluster.label) + ": " + str(cluster.dissimilarCluster)),
		print("   " + str(len(cluster.memberTracks)))
		

			
			
			
			