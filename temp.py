import cPickle as pickle
import random
import warnings
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier
import time

def warn(*args, **kwargs):
	pass
warnings.warn = warn

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []
		
class Cluster:
	def __init__(self, label, tagList):
		self.label = label
		self.tagList = tagList
		self.memberTracks = []
		self.dissimilarCluster = (-1, 0.0)
		
users_dict = {}
tracks_dict = {}
clusters_dict = {}

print("Reading Users")
with open('Users.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		user = pickle.load(infile)
		users_dict[user.username] = user
			   
print("Reading Tracks")
starttime = time.time()
with open('TracksWithLessTags.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		track = pickle.load(infile)
		tracks_dict[track.id] = track
print("Reading tracks: " + str(time.time() - starttime))
print("Generating Clusters")
"""
X = []
i = 0
starttime = time.time()
print("POPULATING X")
for key in tracks_dict:
	track = tracks_dict[key]
	X.append(track.tags)
print("Populating: " + str(time.time() - starttime))
kmeans = KMeans(n_clusters=75, random_state=0, n_jobs=-2)
kmeans = kmeans.fit(X)
print("Clustering : " + str(time.time() - starttime))
"""

with open('Clusters.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		cluster = pickle.load(infile)
		clusters_dict[cluster.label] = cluster

with open('KMeansClassifier.pkl', 'rb') as infile:
	kmeans = pickle.load(infile)


summation = 0
starttime = time.time()
i = 0
for key in users_dict:
	i += 1
	if(i >= 6):
		break
	user = users_dict[key]
	#clf = svm.SVC(kernel='linear')
	#clf = LogisticRegression()
	clf = AdaBoostClassifier()
	tracks = list(set(user.topTracks + user.lovedTracks))
	userTracks = [tracks[i] for i in range(len(tracks)) if tracks[i] in tracks_dict]

	for track in userTracks:
		if(not track in tracks_dict):
			userTracks.remove(track)
	
	X = []
	Y = []
	random.seed()		  
	for track in range(len(userTracks)/2):
		X.append(tracks_dict[userTracks[track]].tags)
		Y.append(1)
		
		currCluster = clusters_dict[kmeans.predict(tracks_dict[userTracks[track]].tags)[0]]
		pos = random.randint(0, len(currCluster.memberTracks) - 1)
		simTrack = currCluster.memberTracks[pos]
		X.append(tracks_dict[simTrack].tags)
		Y.append(1)
		
		pos = random.randint(0, len(currCluster.memberTracks) - 1)
		simTrack = currCluster.memberTracks[pos]
		X.append(tracks_dict[simTrack].tags)
		Y.append(1)
		
		disCluster = clusters_dict[ clusters_dict[kmeans.predict(tracks_dict[userTracks[track]].tags)[0]].dissimilarCluster[0] ]
		pos = random.randint(0, len(disCluster.memberTracks) - 1)
		dissimilarTrack = disCluster.memberTracks[pos]
		X.append(tracks_dict[dissimilarTrack].tags)
		Y.append(0)
		
		pos = random.randint(0, len(disCluster.memberTracks) - 1)
		dissimilarTrack = disCluster.memberTracks[pos]
		X.append(tracks_dict[dissimilarTrack].tags)
		Y.append(0)
		
		pos = random.randint(0, len(disCluster.memberTracks) - 1)
		dissimilarTrack = disCluster.memberTracks[pos]
		X.append(tracks_dict[dissimilarTrack].tags)
		Y.append(0)
		
	if(len(X) > 0 and len(Y) > 0):
		clf.fit(X,Y)
	else:
		continue
	
	count = 0
	#for track in range(len(userTracks)/2 + 1, len(userTracks)):
	for track in range(len(userTracks)/2+1, len(userTracks)):
		testTrack = userTracks[track]
		if(clf.predict(tracks_dict[testTrack].tags)[0] == 1):
			count += 1
		"""
		disCluster = clusters_dict[ clusters_dict[kmeans.predict(tracks_dict[testTrack].tags)[0]].dissimilarCluster[0] ]
		pos = random.randint(0, len(disCluster.memberTracks) - 1)
		dissimilarTrack = disCluster.memberTracks[pos]
		

		if(clf.predict(tracks_dict[dissimilarTrack].tags)[0] == 0):
			count += 1
		"""
	summation += (float(count)*2)/(len(userTracks))
	
print("Total Fit Time: " + str(time.time() - starttime))
#summation = summation/float(len(users_dict) - 46)
summation = summation/6
print(summation)