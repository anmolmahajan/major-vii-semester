import cPickle as pickle
import time
import warnings
from sklearn.cluster import KMeans

def warn(*args, **kwargs):
	pass
warnings.warn = warn

tracks_dict = {}
users_dict = {}

def writeObjectsToFile(name, dictionary):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(dictionary), output)
		for i in dictionary:
			pickle.dump(dictionary[i], output)

def getSimilarity(A,B):
	numerator = 0
	for i in range(len(clusters_dict[A].tagList)):
		numerator += (clusters_dict[A].tagList[i]*clusters_dict[B].tagList[i])
	denA = 0
	denB = 0
	for i in range(len(clusters_dict[A].tagList)):
		denA += (clusters_dict[A].tagList[i]**2)
		denB += (clusters_dict[B].tagList[i]**2)
	denA = denA ** 0.5
	denB = denB ** 0.5
	return numerator/(denA * denB)

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

print("READING TRACKS")
starttime = time.time()
with open("TracksWithLessTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
		
print("READING USERS")
with open("Users.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		users_dict[value.username] = value

print("Reading: " + str(time.time() - starttime))


X = []
i = 0
track_list = []
starttime = time.time()
print("POPULATING X")
for key in tracks_dict:
	track_list.append(key)
	track = tracks_dict[key]
	X.append(track.tags)
print("Populating: " + str(time.time() - starttime))

starttime = time.time()
kmeans = KMeans(n_clusters=75, random_state=0, n_jobs=-2)
kmeans = kmeans.fit(X)
print("Clustering : " + str(time.time() - starttime))
with open('KMeansClassifier2.pkl', 'wb') as output:
		pickle.dump(kmeans, output)

clusters_dict = {}

class Cluster:
	def __init__(self, label, tagList):
		self.label = label
		self.tagList = tagList
		self.memberTracks = []
		self.simCluster = []

for key in tracks_dict:
	track = tracks_dict[key]
	label = kmeans.predict(track.tags)[0]
	
	if(not label in clusters_dict):
		clusters_dict[label] = Cluster(label, track.tags)
	else:
		for i in range(len(track.tags)):
			clusters_dict[label].tagList[i] += track.tags[i]
			
for i in clusters_dict:
	print(i)
	cluster = clusters_dict[i]
	for j in clusters_dict:
		sim = getSimilarity(i, j)
		cluster.simCluster.append((j, sim))
	cluster.simCluster.sort(key=lambda x: x[1],reverse=True)

for i in range(len(kmeans.labels_)):
	clusters_dict[kmeans.labels_[i]].memberTracks.append(track_list[i]) 

writeObjectsToFile("ClustersWithSimList", clusters_dict)



	
	