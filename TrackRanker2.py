import cPickle as pickle
import warnings
import random
from sklearn.cluster import KMeans
from sklearn import svm


def warn(*args, **kwargs):
	pass
warnings.warn = warn

tracks_dict = {}
users_dict = {}
clusters_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

class UserCluster:
	def __init__(self, label):
		self.label = label
		self.memberUsers = []

class Cluster:
	def __init__(self, label, tagList):
		self.label = label
		self.tagList = tagList
		self.memberTracks = []
		self.simCluster = []
		

print("READING TRACKS")
with open("TracksWithLessTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
		
print("READING USERS")
with open("Users.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		users_dict[value.username] = value

with open('ClustersWithSimList.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		cluster = pickle.load(infile)
		clusters_dict[cluster.label] = cluster

with open('KMeansClassifier.pkl', 'rb') as infile:
	kmeans = pickle.load(infile)

for key in users_dict:
	user = users_dict[key]
	user.tags = [0] * 401
	for trackKey in list(set((user.topTracks + user.lovedTracks))):
		if(not trackKey in tracks_dict):
			continue
		track = tracks_dict[trackKey]
		for i in range(len(track.tags)):
			user.tags[i] += track.tags[i]
			
X = []
for key in users_dict:
	user = users_dict[key]
	X.append(user.tags)
kmeansUsers = KMeans(n_clusters=100, random_state=0, n_jobs=-2)
kmeansUsers = kmeansUsers.fit(X)   

clusters_dict_users = {}
for key in users_dict:
	user = users_dict[key]
	label = kmeansUsers.predict(user.tags)[0]
	if(not label in clusters_dict_users):
		clusters_dict_users[label] = UserCluster(label)
	clusters_dict_users[label].memberUsers.append(user.username)

mrr = []

Z = []
c = []
for track in tracks_dict:
	Z.append(tracks_dict[track].tags)
	c.append(tracks_dict[track].id)
	
for key in clusters_dict_users:
	cluster = clusters_dict_users[key]
	clf = svm.LinearSVC()
	X = []
	Y = []
	for member in cluster.memberUsers:
		user = users_dict[member]
		tracks = list(set(user.topTracks + user.lovedTracks))
		userTracks = [tracks[i] for i in range(len(tracks)) if tracks[i] in tracks_dict]
		if(len(userTracks) < 20):
			continue
		for track in userTracks:
			if(not track in tracks_dict):
				userTracks.remove(track)

		for track in range(len(userTracks)):
			X.append(tracks_dict[userTracks[track]].tags)
			Y.append(1)
			#print(tracks_dict[userTracks[track]].name.encode('utf-8').strip())
			disCluster = clusters_dict[ clusters_dict[kmeans.predict(tracks_dict[userTracks[track]].tags)[0]].simCluster[-1][0] ]
			
			while(True) :
				pos = random.randint(0, len(disCluster.memberTracks) - 1)
				dissimilarTrack = disCluster.memberTracks[pos]
				if(not dissimilarTrack in userTracks):
					break

			X.append(tracks_dict[dissimilarTrack].tags)
			Y.append(0)
			
	if(len(X) > 0 and len(Y) > 0):
		clf.fit(X,Y)
	else:
		continue
		
	rank_list = clf.decision_function(Z)
	rank_list = rank_list.tolist()
	for i in range(len(rank_list)):
		rank_list[i] = (rank_list[i], c[i])
	rank_list.sort(key=lambda x: x[0],reverse=True)
	
	for member in cluster.memberUsers:
		user = users_dict[member]
		tracks = list(set(user.topTracks + user.lovedTracks))
		userTracks = [tracks[i] for i in range(len(tracks)) if tracks[i] in tracks_dict]
		if(len(userTracks) > 20):
			continue
		count = 0
		for i in range(len(rank_list)):
			track = rank_list[i][1]
			count += 1
			if(track in userTracks):
				break
		print(str(count) + " "),
		mrr.append(count)

from scipy import stats
print("\n\n")
print(stats.hmean(mrr))