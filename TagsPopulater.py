import cPickle as pickle

tracks_dict = {}
tags_dict = {}

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

class Tag:
	def __init__(self, name):
	  self.name = name
	  self.associatedSongs = []

def readObjectsFromFile(name, dict_name):
	with open(name + ".pkl", "rb") as infile:
		for i in range(pickle.load(infile)):
			value = pickle.load(infile)
			dict_name[value.id] = value

def populateTags():
	for track in tracks_dict:
		for item in tracks_dict[track].tags:
			if(not item in tags_dict):
				tag = Tag(item)
				tags_dict[item] = tag
			tags_dict[item].associatedSongs.append(tracks_dict[track].id)

def writeObjectsToFile(name, dictionary):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(tags_dict), output)
		for key in dictionary:
			pickle.dump(dictionary[key], output)

print("START")		
readObjectsFromFile("Tracks-Final", tracks_dict)
print("DONE READING")
populateTags()
print("POPULATED")
writeObjectsToFile("Tags", tags_dict)