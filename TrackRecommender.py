import cPickle as pickle
import random
import warnings
from sklearn import svm
import time

def warn(*args, **kwargs):
	pass
warnings.warn = warn

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []
		
class Cluster:
	def __init__(self, label, tagList):
		self.label = label
		self.tagList = tagList
		self.memberTracks = []
		self.dissimilarCluster = (-1, 0.0)
		
users_dict = {}
tracks_dict = {}
clusters_dict = {}

print("Reading Users")
with open('Users.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		user = pickle.load(infile)
		users_dict[user.username] = user
			   
print("Reading Tracks")
starttime = time.time()
with open('TracksWithLessTags.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		track = pickle.load(infile)
		tracks_dict[track.id] = track
print("Generating Clusters")
"""
X = []
i = 0
starttime = time.time()
print("POPULATING X")
for key in tracks_dict:
	track = tracks_dict[key]
	X.append(track.tags)
print("Populating: " + str(time.time() - starttime))
kmeans = KMeans(n_clusters=75, random_state=0, n_jobs=-2)
kmeans = kmeans.fit(X)
print("Clustering : " + str(time.time() - starttime))
"""

with open('Clusters.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		cluster = pickle.load(infile)
		clusters_dict[cluster.label] = cluster

with open('KMeansClassifier.pkl', 'rb') as infile:
	kmeans = pickle.load(infile)


summation = 0
usercount = 0
starttime = time.time()
for key in users_dict:
	user = users_dict[key]
	clf = svm.SVC(kernel='linear')
	
	tracks = list(set(user.topTracks + user.lovedTracks))
	userTracks = [tracks[i] for i in range(len(tracks)) if tracks[i] in tracks_dict]
	if(len(userTracks) < 20):
		continue

	for track in userTracks:
		if(not track in tracks_dict):
			userTracks.remove(track)
	
	X = []
	Y = []
			  
	for track in range(len(userTracks)/2):
		X.append(tracks_dict[userTracks[track]].tags)
		Y.append(1)
		#print(tracks_dict[userTracks[track]].name.encode('utf-8').strip())
		disCluster = clusters_dict[ clusters_dict[kmeans.predict(tracks_dict[userTracks[track]].tags)[0]].dissimilarCluster[0] ]
		
		while(True) :
			pos = random.randint(0, len(disCluster.memberTracks) - 1)
			dissimilarTrack = disCluster.memberTracks[pos]
			if(not dissimilarTrack in userTracks):
				break
		

		X.append(tracks_dict[dissimilarTrack].tags)
		Y.append(0)
	if(len(X) > 0 and len(Y) > 0):
		clf.fit(X,Y)
	else:
		continue

	usercount += 1
	count = 0
	for track in range(len(userTracks)/2 + 1, len(userTracks)):
	  testTrack = userTracks[track]
	  if(clf.predict(tracks_dict[testTrack].tags)[0] == 1):
			count += 1
	summation += float(count)/(len(userTracks)/2.0)

print("Total Time: " + str(time.time() - starttime))
summation = (summation/float(usercount))*100
print("Mean Accuracy: " + str(summation) + " %")