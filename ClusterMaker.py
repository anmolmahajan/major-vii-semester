import cPickle as pickle
import time
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans

tracks_dict = {}
users_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

print("READING TRACKS")
starttime = time.time()
with open("TracksWithLessTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
		
print("READING USERS")
with open("Users.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		users_dict[value.username] = value

print("Reading: " + str(time.time() - starttime))


X = []
i = 0
starttime = time.time()
print("POPULATING X")
for key in tracks_dict:
	track = tracks_dict[key]
	X.append(track.tags)
print("Populating: " + str(time.time() - starttime))

"""
starttime = time.time()
pca = PCA(n_components=200, svd_solver='randomized')
X = pca.fit_transform(X)
print("PCA: " + str(time.time() - starttime))
"""
starttime = time.time()
kmeans = KMeans(n_clusters=75, random_state=0, n_jobs=-2)
kmeans = kmeans.fit(X)
print("Clustering : " + str(time.time() - starttime))

clusters_dict = {}

f = open('KMeansClassifier.pkl', 'wb')
pickle.dump(kmeans, f)
f.close()

print(kmeans.predict(tracks_dict['48050803-afd1-4420-8667-6ad4ffab65ac'].tags))

f = open('KMeansClassifier.pkl', 'rb')
kmeansRead = pickle.load(f)
f.close()

print(kmeansRead.predict(tracks_dict['48050803-afd1-4420-8667-6ad4ffab65ac'].tags))

"""
for key in tracks_dict:
	track = tracks_dict[key]
	label = kmeans.predict(track.tags)
	if(not label in clusters_dict):
		clusters_dict[label] = track.tags
	else:
		for i in range(track.tags):
			clusters_dict[label][i] += track.tags[i]
"""

	

	
	