import cPickle as pickle

class Tag:
		def __init__(self, name):
			self.name = name
			self.associatedSongs = []

tags_dict = {}
new_dict = {}

with open("Tags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tags_dict[value.name] = value
				

def writeObjectsToFile(name, dictionary):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(new_dict), output)
		for key in dictionary:
			pickle.dump(dictionary[key], output)
				
tags_list = ['Blues', 'Children', 'Classical', 'Electronic', 'Folk', 'Funk', 'Jazz', 'Latin', 'Pop', 
						 'hop', 'Rock', 'Reggae', 'Rock', 'Soul', 'RnB', 'R&B', 'r n b', 'r & b', 'country', 'dance',
						 'edm', 'alternative', 'indie', 'mood', 'instrumental', 'punk', 'metal', 'soundtrack', 'Religious']

print(len(tags_dict))

for key in tags_dict:
		tag = tags_dict[key]
		for i in tags_list:
				if(i.lower() in tag.name.lower()):
						new_dict[key] = tag
						break
				
print(len(new_dict))
writeObjectsToFile("TrimmedTags", new_dict)
