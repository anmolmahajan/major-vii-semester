import cPickle as pickle
import time
import warnings
import random
from sklearn.cluster import KMeans
from sklearn import svm


def warn(*args, **kwargs):
	pass
warnings.warn = warn

tracks_dict = {}
users_dict = {}
clusters_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []
		
class Cluster:
	def __init__(self, label, tagList):
		self.label = label
		self.tagList = tagList
		self.memberTracks = []
		self.simCluster = []
		
def getSimilarity(A,B):
	numerator = 0
	for i in range(len(A)):
		numerator += (A[i]*B[i])
	denA = 0
	denB = 0
	for i in range(len(A)):
		denA += A[i]**2
		denB += B[i]**2
	denA = denA ** 0.5
	denB = denB ** 0.5
	return numerator/(denA * denB)
		
print("READING TRACKS")
starttime = time.time()
with open("TracksWithLessTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
		
print("READING USERS")
with open("Users.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		users_dict[value.username] = value

with open('ClustersWithSimList.pkl', 'rb') as infile:
	for i in range(pickle.load(infile)):
		cluster = pickle.load(infile)
		clusters_dict[cluster.label] = cluster

with open('KMeansClassifier2.pkl', 'rb') as infile:
	kmeans = pickle.load(infile)


for key in users_dict:
	user = users_dict[key]
	user.tags = [0] * 401
	for trackKey in list(set((user.topTracks + user.lovedTracks))):
		if(not trackKey in tracks_dict):
			continue
		track = tracks_dict[trackKey]
		for i in range(len(track.tags)):
			user.tags[i] += track.tags[i]

for key in users_dict:
	user = users_dict[key]
	clf = svm.LinearSVC()
	tracks = list(set(user.topTracks + user.lovedTracks))
	userTracks = [tracks[i] for i in range(len(tracks)) if tracks[i] in tracks_dict]

	for track in userTracks:
		if(not track in tracks_dict):
			userTracks.remove(track)
	
	X = []
	Y = []
			  
	for track in range(len(userTracks)):
		X.append(tracks_dict[userTracks[track]].tags)
		Y.append(1)
		#print(tracks_dict[userTracks[track]].name.encode('utf-8').strip())
		disCluster = clusters_dict[ clusters_dict[kmeans.predict(tracks_dict[userTracks[track]].tags)[0]].simCluster[-1][0] ]
		dissimilarTrack = None
		while(True):
			pos = random.randint(0, len(disCluster.memberTracks) - 1)
			dissimilarTrack = disCluster.memberTracks[pos]
			if(not dissimilarTrack in userTracks):
				break
		X.append(tracks_dict[dissimilarTrack].tags)
		Y.append(0)
	if(len(X) > 0 and len(Y) > 0):
		clf.fit(X,Y)
	else:
		continue
	
	maxD = -100000000
	userCluster = None
	for key in clusters_dict:
		s = getSimilarity(user.tags, clusters_dict[key].tagList)
		if(s > maxD):
			maxD = s
			userCluster = clusters_dict[key]
	
	X = []
	y = []
	for i in range(1):
		currClusterTracks = clusters_dict[userCluster.simCluster[i][0]].memberTracks
		for j in range(len(currClusterTracks)):
			X.append(tracks_dict[currClusterTracks[j]].tags)
			y.append(tracks_dict[currClusterTracks[j]].id)
	rank_list = clf.decision_function(X)
	rank_list = rank_list.tolist()
	for i in range(len(rank_list)):
		rank_list[i] = (rank_list[i], y[i])
	rank_list.sort(key=lambda x: x[0],reverse=True)
	count = 0
	flag = False
	for i in rank_list:
		count += 1
		if(i[1] in userTracks):
			flag = True
			break
	if flag:
			print(count)
	break

	