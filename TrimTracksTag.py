import cPickle as pickle

class Tag:
		def __init__(self, name):
			self.name = name
			self.associatedSongs = []
			
class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

tracks_dict = {}
new_dict = {}

with open("Tracks-Final.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
				

def writeObjectsToFile(name, dictionary):
	with open(name + '.pkl', 'wb') as output:
		pickle.dump(len(new_dict), output)
		for key in dictionary:
			pickle.dump(dictionary[key], output)
				
tags_list = ['Blues', 'Children', 'Classical', 'Electronic', 'Folk', 'Funk', 'Jazz', 'Latin', 'Pop', 
						 'hop', 'Rock', 'Reggae', 'Rock', 'Soul', 'RnB', 'R&B', 'r n b', 'r & b', 'country', 'dance',
						 'edm', 'alternative', 'indie', 'mood', 'instrumental', 'punk', 'metal', 'soundtrack', 'Religious']



for key in tracks_dict:
		track = tracks_dict[key]
		temp = []
		for tag in track.tags:
				for i in tags_list:
						if(i.lower() in tag.lower()):
								temp.append(tag)
								break
		track.tags = temp
		new_dict[track.id] = track
				
print(len(new_dict))
writeObjectsToFile("TrimmedTagsTracks", new_dict)
