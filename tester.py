import cPickle as pickle
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
import numpy as np
import time

users_dict = {}
tracks_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []
	  
print("READING USERS")
with open("../Users.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		users_dict[value.username] = value
print("READING TRACKS")



with open("../DenseTrimmedTagsTracks.pkl", "rb") as infile:
	pickle.load(infile)
	for i in range(5000):
		value = pickle.load(infile)
		tracks_dict[value.id] = value

#for key in users_dict:
#user = users_dict[key]
#user = users_dict["RJ"]

X = []
Y = []
print("FILLING X")

"""
for track in list(set(user.topTracks + user.lovedTracks)):
	if(len(tracks_dict[track].tags) > 0):
		X.append(tracks_dict[track].tags)
"""
from sklearn.decomposition import SparsePCA
pca = SparsePCA(n_components=100)
starttime = time.time()
for key in tracks_dict:
	track = tracks_dict[key]
	X.append(track.tags)

print("Time to populate X: " + str(time.time() - starttime))

starttime = time.time()
X = pca.fit_transform(X)
print("Time for PCA: " + str(time.time() - starttime))


#kmeans = KMeans(n_clusters=10, random_state=0).fit(X)
#print(kmeans.labels_)

