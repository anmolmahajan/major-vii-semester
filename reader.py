import cPickle as pickle

users_dict = {}
tracks_dict = {}

class User:
	def __init__(self, username):
		self.username = username
		self.friends = []
		self.lovedTracks = []
		self.topTracks = []

class Track:
	def __init__(self, id):
		self.id = id
		self.artistId = ""
		self.name = ""
		self.tags = []

print("READING TRACKS")
with open("TracksWithLessTags.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		tracks_dict[value.id] = value
		
print("READING USERS")
with open("Users.pkl", "rb") as infile:
	for i in range(pickle.load(infile)):
		value = pickle.load(infile)
		users_dict[value.username] = value


usercount = 0
inverse_rank_list = []
for key in users_dict:
	user = users_dict[key]
	tracks = list(set(user.topTracks + user.lovedTracks))
	userTracks = [tracks[i] for i in range(len(tracks)) if tracks[i] in tracks_dict]
	if(len(userTracks) < 20):
		continue
	usercount += 1
	print(usercount)
	with open("User Ranks/" + key + "ranklist.pkl", "rb") as infile:
		my_list = pickle.load(infile)
	
	count = 0
	for i in my_list:
		count += 1
		if(i[1] in userTracks):
			break
	inverse_rank_list.append(count)

from scipy import stats
print stats.hmean(inverse_rank_list)





